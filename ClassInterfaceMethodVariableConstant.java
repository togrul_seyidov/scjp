﻿
/**
* @author Togrul Seyid
* @version 1.0
* #meqsed:) : Java Code Conventions
*/
public class ClassInterfaceMethodVariableConstant   // Class Adi bele olmalidi Boyuk herflerle bashlamalidi
{
	public static final float MY_MONEY = 0.60f;   // Constantlar Boyuk herflerle yazilir eger cox sozduse aralarinda _ atilir:   JAVA_HOME
	public static void main(String[] args)
	{
		doGreeting(); // methodu caqirdiq
		
	}
	
	public static void doGreeting() // Method adlari   'set', 'get', 'do', 'is' ve sair bashliqlari ile bashlayir ve sonraki soz boyuk yazilir
	{
		String myName= "Togrul Seyidov"; // Deyishkenler kicik herfle bashlayir ve ikinci soz boyuk herfle yazilir
		int    myAge = 21;
		System.out.println("Əssalamun Aleykum :)"
				+"\nMy name is:" + myName
				+ "\nMy age:" + myAge
				+"\nI have " +MY_MONEY +" manat" );
	}
}


